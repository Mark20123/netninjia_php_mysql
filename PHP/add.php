<?php
 $email=$title=$ingredients='';

 $errors = array('email'=>'','title'=>'','ingredients'=>'');
// user submits a form

 if(isset($_POST['submit'])){
     
     
     // the array itself
     
    echo print_r($_POST).'<br />';
     
     
     ///check email if not empty
     
     if(empty($_POST['email'])){
         $errors['email'] = '!an email is required is empty <br />';
     }else{
      $email = $_POST["email"];
      if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
          $errors['email'] = 'email must be valid address <br />';
      }    
     }
     
     
     //check title not empty
     
     if(empty($_POST['title'])){
         $errors['title'] = '!Error title empty <br />';
     }else{
       $title = $_POST['title'];
       if(!preg_match('/^[a-zA-Z\s]+$/',$title)){
           $errors['title'] = 'Title must be letters only <br />';
            }   
     }
     
     
     //check ingredients not empty
     
     if(empty($_POST['ingredients'])){
         $errors['ingredients'] = '!Error ingredients empty <br />';
     }else{
           $ingredients = $_POST['ingredients'];
           if(!preg_match('/^[a-zA-Z\s]+(,\s[a-zA-Z\s]*)*$/',$title)){
               $errors['ingredients'] = 'Ingredients must contains letters only <br />';
                } 
     }
     
     if(array_filter($errors)){
         //echo 'Errors in the forms';
     }else{
         header('Location: index.php');
     }
     
 } //end of post check
?>


<!DOCTYPE html>
<html lang="en">
    
    <?php include('header.php');?>
    
    <section class="container grey-text">
       <h4 class="center">Add a Pizza</h4>
       <form class="white" action="add.php" method="POST">
         <label>Your email:</label>
         <input type="text" name="email" value="<?php echo $email; ?>">
         <div class="red-text"><?php echo $errors['email'];?></div>
         <label>Pizza Title</label>
         <input type="text" name="title" value="<?php echo $title; ?>">  
         <div class="red-text"><?php echo $errors['title'];?></div>
         <label>Ingredients (comma separated)</label>
         <input type="text" name="ingredients" value="<?php echo $ingredients; ?>">  
         <div class="red-text"><?php echo $errors['ingredients']; ?></div>
         <div class="center">
             <input type="submit" name="submit" value="submit" class="btn brand z-depth-0">
         </div>   
       </form>
    </section>
    <?php include('footer.php');?>

</html>
    