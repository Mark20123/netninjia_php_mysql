<head>
<title>Ninjia Pizza</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="css/custom.css" rel="stylesheet">    
</head>
<body class="grey lighten-4">
    <nav class="white z-depth-0">
        <div class="container">
            <a href="#" class="brand-logo brand-text">Ninjia Pizza
            </a>
            <ul id="nav-mobile" class="right hide-on-small-and-down">
            <li><a href="#" class="btn brand z-depth-0">Add a Pizza</a></li>
            </ul>
        </div>
    </nav>